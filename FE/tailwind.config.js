/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors: {
      primary: "#0a9830 !important",
    },
    extend: {
      backgroundColor: {
        "primary-background-content-color": "#fff",
      },
      boxShadow: {
        "primary-background-color": "0 1px 0 #f6f8fb",
      },
    },
  },
  plugins: [],
};
