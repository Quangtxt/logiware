import React, { useContext, useState, Fragment } from "react";
import PropTypes from "prop-types";

// import MainSidebar from "../../components/MainSidebar";
import MainHeaderBar from "../../components/MainHeaderBar";
import SearchBox from "../../components/SearchBox";
import { StoreContext } from "../../stores/storeContext";

import MainFooterBar from "../../components/MainFooterBar";
import SlideBar from "../../components/SlideBar";

import { ContentWrapper, LayoutWrapper, MainWrapper } from "./UserLayoutStyled";
const UserLayout = (props) => {
  const { children, title, showFooter, showSlide } = props;
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <Fragment>
      <MainHeaderBar title={title} />
      {/* <SearchBox activeIndex={activeIndex} setActiveIndex={setActiveIndex} />
      {activeIndex === 0 ? <WarehousesPage /> : <></>} */}
      {showSlide && <SlideBar />}
      <MainWrapper>
        <LayoutWrapper>
          <ContentWrapper style={{ width: "100%" }}>{children}</ContentWrapper>
        </LayoutWrapper>
      </MainWrapper>
      {showFooter && <MainFooterBar />}
    </Fragment>
  );
};

UserLayout.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};

export default UserLayout;
