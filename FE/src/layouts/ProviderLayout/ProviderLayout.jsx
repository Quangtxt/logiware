import React, { useContext, useState, Fragment, useCallback } from "react";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  LogoutOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Button, Layout, Menu, theme, Dropdown, Avatar } from "antd";
import { green } from "../../color";
import { Logo } from "../../components/MainHeaderBar/MainHeaderBarStyled";
import logoImage from "../../assets/logo.jpg";
import utils from "../../utils";
import { subStringAvatar } from "../../components/Common/CellText";
import PageTitle from "../../components/PageTitle/PageTitle";
import { useLocation, useNavigate } from "react-router-dom";
const { Header, Sider, Content } = Layout;

const ProviderLayout = (props) => {
  const { children } = props;
  const location = useLocation();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  const navigate = useNavigate();
  const [currentUser, setCurrentUser] = useState(true);
  const clickLogout = useCallback(() => {
    navigate("/");
    setCurrentUser(false);
  }, []);
  const getSelectedKey = () => {
    if (location.pathname.startsWith("/provider/warehouse")) {
      return "warehouse";
    } else if (location.pathname.startsWith("/provider/company")) {
      return "company";
    } else {
      return "1";
    }
  };
  const items = [
    {
      key: "1",
      label: (
        <div
          style={{ color: "blue", cursor: "pointer" }}
          onClick={() => navigate("/profile")}
        >
          Trang chủ
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          style={{ color: "red", cursor: "pointer" }}
          onClick={() => clickLogout()}
        >
          <LogoutOutlined style={{ marginRight: "5px" }} />
          Đăng Xuất
        </div>
      ),
      danger: true,
    },
  ];
  return (
    <Layout style={{ height: "100vh" }}>
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        style={{ background: colorBgContainer }}
      >
        {!collapsed ? (
          <div className="mb-8">
            <div className="grid">
              <Logo href="/">
                <img src={logoImage} alt="Your Company" />
                <div>LOGIWARE</div>
              </Logo>
              <span className="flex justify-center text-12px tracking-[2px] text-primary">
                Partner
              </span>
            </div>
          </div>
        ) : (
          <div className="flex justify-center items-center mb-8">
            <Logo href="/">
              <img src={logoImage} alt="Your Company" />
            </Logo>
          </div>
        )}

        <Menu
          mode="inline"
          defaultSelectedKeys={[getSelectedKey()]}
          items={[
            {
              key: "company",
              icon: <UserOutlined />,
              label: "Công ty",
              style: { marginBottom: "16px" },
              onClick: () => navigate("/provider/company"),
            },
            {
              key: "warehouse",
              icon: <VideoCameraOutlined />,
              label: "Kho dịch vụ",
              onClick: () => navigate("/provider/warehouse"),
            },
          ]}
        />
      </Sider>
      <Layout>
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div className="flex items-center">
            <Button
              type="text"
              icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              onClick={() => setCollapsed(!collapsed)}
              style={{
                fontSize: "16px",
                width: 64,
                height: 64,
              }}
            />
            <div className="flex-1 mt-[12px]">
              <PageTitle location={location}></PageTitle>
            </div>
          </div>

          <div className="flex mr-8">
            <Dropdown
              menu={{ items }}
              placement="topLeft"
              trigger={["click"]}
              className="sidebarItem"
            >
              <div className="flex items-center hover:cursor-pointer">
                <Avatar
                  size={28}
                  style={{ backgroundColor: green, fontSize: 12 }}
                >
                  {/* {currentUser && subStringAvatar(currentUser.username)} */}
                  {subStringAvatar("Nguyen Van A")}
                </Avatar>
                <span
                  style={{
                    margin: "0 7px",
                    fontWeight: 500,
                  }}
                >
                  Mr {utils.getNameInCapitalize("Nguyen Van A")}
                </span>
              </div>
            </Dropdown>
          </div>
        </Header>
        <Content className="overflow-auto relative bg-primary-background-color h-full p-[16px]">
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default ProviderLayout;
