import { makeAutoObservable } from "mobx";

class LoadingAnimationStore {
  loadingQueue = [];
  isVisible = this.loadingQueue.length !== 0;
  tableLoading = false;
  showSpinInline = false;

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  showSpinner = (state) => {
    if (state) {
      this.loadingQueue.push(1);
    } else {
      this.loadingQueue.pop();
    }
    this.updateVisibility();
  };

  setTableLoading = (state) => {
    this.tableLoading = state;
  };

  clearStore = () => {
    this.tableLoading = false;
    this.isVisible = false;
    this.loadingQueue.length = 0;
  };

  setShowSpinInline = (state) => {
    this.showSpinInline = state;
  };

  updateVisibility = () => {
    this.isVisible = this.loadingQueue.length !== 0;
  };
}

export default new LoadingAnimationStore();
