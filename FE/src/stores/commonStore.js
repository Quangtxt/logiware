import { useState, useEffect } from "react";
import date_utils from "../date_utils";

const CommonStore = () => {
  const [pageName, setPageName] = useState([]);
  const [appTheme, setAppTheme] = useState({
    name: "blue",
    solidColor: "#1890FF",
    solidLightColor: "#1890FF",
    gradientColor: "#1890FF",
  });
  const [openedSubMenu, setOpenedSubMenu] = useState([]);
  const [isSidebarCollapsed, setIsSidebarCollapsed] = useState(false);
  const [weekChange, setWeekChange] = useState(
    date_utils.weekRange(date_utils.current())
  );
  const [collapsedMenu, setCollapsedMenu] = useState(true);

  const setPage = (newPageName) => {
    setPageName(newPageName);
  };

  const toggleCollapsedSidebar = (state) => {
    setIsSidebarCollapsed(state);
  };

  const clearStore = () => {
    setPageName([]);
    setIsSidebarCollapsed(false);
  };

  const setWeekChangeState = (newWeekChange) => {
    setWeekChange(newWeekChange);
  };

  const toggleCollapsedMenu = () => {
    setCollapsedMenu(!collapsedMenu);
  };

  useEffect(() => {
    // Add any necessary side effects or initialization here
  }, []);

  return {
    pageName,
    setPage,
    appTheme,
    openedSubMenu,
    setOpenedSubMenu,
    isSidebarCollapsed,
    toggleCollapsedSidebar,
    mouseCordinate,
    setMouseCordinate,
    clearMouseCordinate,
    clearStore,
    weekChange,
    setWeekChangeState,
    collapsedMenu,
    toggleCollapsedMenu,
  };
};

export default CommonStore;
