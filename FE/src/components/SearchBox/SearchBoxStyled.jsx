import styled, { css } from "styled-components";

export const BackGround = styled.div`
  min-height: 556px;
  margin: 0;
  padding: 32px;
  background-size: cover;
  background-image: linear-gradient(
      0deg,
      rgba(1, 44, 21, 0.4),
      rgba(1, 44, 21, 0.4)
    ),
    url(${({ background_image }) => background_image});

  abs {
    max-width: 1200px;
    margin: auto;
  }
  .tabs .ant-tabs-tab {
    border: none !important;
    width: 320px;
    height: 56px;
    transition: none !important;
  }

  .tabs .ant-tabs-nav {
    margin-bottom: 0;
  }

  .tabs .ant-tabs-nav:before {
    border-bottom: none;
  }

  .tabs .ant-tabs-tab-btn {
    color: #fff;
    font-size: 20px;
    font-weight: 700;
    width: 100%;
    text-align: center;
    transition: none !important;
  }

  .tabs .ant-tabs-tab-active {
    background: hsla(0, 7%, 8%, 0.9) !important;
  }

  .tabs .ant-tabs-content-holder {
    background: hsla(0, 7%, 8%, 0.9);
    height: -moz-fit-content;
    height: fit-content;
    border-radius: 0 8px 8px 8px;
  }
  .tabs .ant-tabs-nav .ant-tabs-ink-bar {
    visibility: hidden;
  }
  @media only screen and (max-width: 480px) {
    .tabs .ant-tabs-tab {
      width: -moz-fit-content;
      width: fit-content;
      height: 48px;
      padding: 8px !important;
    }

    .tabs .ant-tabs-tab-btn {
      font-size: 13px;
      font-weight: 500;
      width: 100%;
    }
  }
`;

export const Introduction = styled.h1`
  font-size: 60px;
  font-weight: 700;
  color: #fff;
  margin: auto auto 24px;
  padding-top: 40px;
  text-align: center;
  span {
    color: #00e73e;
  }
`;

export const TabButton = styled.div`
  display: flex;
  justify-content: center;
  color: white;
  .anticon {
    svg {
      width: 1em;
      height: 1em;
    }
  }
`;

export const ExploreMarket = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
  padding: 24px 32px;
  align-items: center;
`;

export const HeaderExplore = styled.span`
  color: #fff;
  font-size: 16px;
  max-width: 500px;
  text-align: center;
  font-style: italic;
`;

export const ExploreContentList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  gap: 6rem;
  padding: 0 6rem;

  @media only screen and (min-width: 320px) {
    gap: 2rem;
    padding: 0 2rem;
  }

  @media only screen and (min-width: 480px) {
    gap: 2rem;
    padding: 0 2rem;
  }

  @media only screen and (min-width: 768px) {
    gap: 2rem;
    padding: 0 2rem;
  }

  @media only screen and (min-width: 1024px) {
    gap: 2rem;
    padding: 0 3rem !important;
  }

  @media only screen and (min-width: 1280px) {
    gap: 3rem;
    padding: 0 4rem !important;
  }

  @media only screen and (min-width: 1536px) {
    gap: 4rem;
    padding: 0 5rem;
  }
`;

export const ExploreContent = styled.div`
  height: 86px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  color: #fff;
  gap: 8px;
  width: 100px;
  text-align: center;
  font-weight: 600;

  &:hover {
    color: #00e73e;
  }
  .icon {
    font-size: 30px;
  }
  ${({ active }) =>
    active &&
    `
    color: #00e73e;
  `}
`;

export const ContentDivider = styled.div`
  width: 1px;
  height: 98px;
  border-right: 2px solid #d9d9d9;
  @media only screen and (max-width: 768px) {
    display: none;
  }
`;
