import React, {
  memo,
  useCallback,
  useEffect,
  useState,
  useContext,
} from "react";
import PropTypes from "prop-types";
// Component

// Styled Component

// Ant design
import { Dropdown, Menu, message, Tabs, Tooltip, Row, Col, Badge } from "antd";
import {
  TruckFilled,
  AppstoreOutlined,
  LogoutOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faStore,
  faStoreSlash,
  faWater,
} from "@fortawesome/free-solid-svg-icons";
// Other
import { inject, observer } from "mobx-react";
import { blue } from "../../color";
import HeroImage from "../../assets/hero.png";
// import { subStringAvatar } from "../Common/CellText";

import { StoreContext } from "../../stores/storeContext";

import {
  BackGround,
  Introduction,
  TabButton,
  ExploreMarket,
  HeaderExplore,
  ExploreContentList,
  ExploreContent,
  ContentDivider,
} from "./SearchBoxStyled";

const { TabPane } = Tabs;

const SearchBox = (props) => {
  // const {
  //   authenticationStore,
  //   accountStore,
  //   history,
  //   companyStore,
  //   userStore,
  //   notificationStore,
  // } = props;

  const clickLogout = useCallback(() => {
    // accountStore.clearStore();
    // companyStore.clearStore();
    // userStore.clearStore();
    // notificationStore.clearStore();
    // authenticationStore.userLogout();
    history.replace("/");
  }, []);

  const menu = (
    <Menu>
      <Menu.Item
        onClick={() => history.push("/profile")}
        style={{ color: blue }}
      >
        <UserOutlined style={{ color: blue, marginRight: "5px" }} />
        Thông Tin Cá Nhân
      </Menu.Item>
      <Menu.Item onClick={() => clickLogout()} danger>
        <LogoutOutlined style={{ marginRight: "5px" }} />
        Đăng Xuất
      </Menu.Item>
    </Menu>
  );
  const setContent = useCallback((index) => {
    setActiveIndex(index);
  }, []);
  return (
    <BackGround background_image={HeroImage}>
      <Row style={{ marginLeft: -8, marginRight: -8, rowGap: 16 }}>
        <Col xs={24}>
          <Introduction>
            Nền Tảng Logistics Đa Dịch Vụ <br />
            <span>Đầu Tiên tại Việt Nam</span>
          </Introduction>
        </Col>
        <Col xs={24}>
          <Tabs
            defaultActiveKey="marketplace"
            className="tabs"
            items={[
              // {
              //   key: "quotes",
              //   label: (
              //     <TabButton>
              //       <AppstoreOutlined />
              //       <span>Nhận nhiều báo giá</span>
              //     </TabButton>
              //   ),
              //   children: <div>Báo giá</div>,
              // },
              {
                key: "marketplace",
                label: (
                  <TabButton>
                    <AppstoreOutlined />
                    <span>Khám phá thị trường</span>
                  </TabButton>
                ),
                children: (
                  <ExploreMarket>
                    <HeaderExplore>
                      Khám phá giải pháp logistics tương lai của bạn cùng
                      Wareflex
                    </HeaderExplore>
                    <ExploreContentList>
                      <ExploreContent
                        onClick={() => setContent(0)}
                        active={activeIndex === 0}
                      >
                        <FontAwesomeIcon icon={faStore} className="icon" />
                        <div>Kho dịch vụ</div>
                      </ExploreContent>
                      <ContentDivider />
                      <ExploreContent
                        onClick={() => setContent(1)}
                        active={activeIndex === 1}
                      >
                        <FontAwesomeIcon icon={faStoreSlash} className="icon" />
                        <div>Kho không dịch vụ</div>
                      </ExploreContent>
                      <ContentDivider />
                      <ExploreContent
                        onClick={() => setContent(2)}
                        active={activeIndex === 2}
                      >
                        <TruckFilled className="icon" />
                        <div>Vận chuyển</div>
                      </ExploreContent>
                      <ContentDivider />
                      <ExploreContent
                        onClick={() => setContent(3)}
                        active={activeIndex === 3}
                      >
                        <FontAwesomeIcon icon={faWater} className="icon" />
                        <div>Khác</div>
                      </ExploreContent>
                    </ExploreContentList>
                  </ExploreMarket>
                ),
              },
            ]}
          />
        </Col>
      </Row>
    </BackGround>
  );
};

export default SearchBox;
