import styled, { css } from "styled-components";

export const HeaderContainer = styled.header`
  // width: 100%;
  height: 60px;
  display: flex;
  background-color: white;
  position: sticky;
  padding: 0 1.5rem;
  z-index: 1000;
  top: 0;
  justify-content: space-between;
`;

export const HeaderContent = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Logo = styled.a`
  display: flex;
  align-items: center;
  text-decoration: none;
  img {
    height: 60px !important;
    width: auto;
  }
  div {
    font-size: 1.25rem;
    font-weight: 700;
  }
`;

export const LanguageSelector = styled.div`
  display: none;
  align-items: center;
  color: #718096;
  cursor: pointer;

  img {
    height: 1.25rem;
    width: auto;
    margin-right: 0.5rem;
  }

  svg {
    margin-left: 0.5rem;
    color: #a0aec0;
  }

  @media (min-width: 768px) {
    display: flex;
  }
`;

export const AuthButtons = styled.div`
  display: none;
  align-items: center;
  gap: 1rem;

  @media (min-width: 768px) {
    display: flex;
  }
`;
export const ToDashBoard = styled.div`
  border-radius: 22px;
  height: 28px;
  padding: 4px 12px;
  gap: 8px;
  margin: 0 20px;
  border: #0a9830;
  color: #fff !important;
  background: linear-gradient(90deg, #00e73e 0.07%, #1c983d 99.95%);
`;

export const LoginButton = styled.a`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem 1rem;
  border: 1px solid black;
  border-radius: 0.375rem;
  color: black;
  font-size: 0.875rem;
  font-weight: 500;
  background-color: white;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    background-color: #f7fafc;
  }
`;

export const RegisterButton = styled.a`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem 1rem;
  border: 1px solid black;
  border-radius: 0.375rem;
  color: black;
  font-size: 0.875rem;
  font-weight: 500;
  background-color: white;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    background-color: #e2e8f0;
  }
`;

export const MobileMenuButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem;
  border-radius: 0.375rem;
  color: #718096;
  transition: all 0.2s ease-in-out;

  &:hover {
    background-color: #f7fafc;
  }

  @media (min-width: 768px) {
    display: none;
  }
`;
