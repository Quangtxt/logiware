import React, {
  memo,
  useCallback,
  useEffect,
  useState,
  useContext,
} from "react";
import PropTypes from "prop-types";
// Component

// Styled Component

// Ant design
import { Avatar, Badge, Dropdown } from "antd";
import { LogoutOutlined } from "@ant-design/icons";

// Other
import { inject, observer } from "mobx-react";
import { blue, green } from "../../color";
import logoImage from "../../assets/logo.jpg";
import { subStringAvatar } from "../Common/CellText";
import utils from "../../utils";
import {
  HeaderContainer,
  Logo,
  AuthButtons,
  LoginButton,
  RegisterButton,
  MobileMenuButton,
  ToDashBoard,
} from "./MainHeaderBarStyled";
import { useNavigate, Link } from "react-router-dom";

const MainHeaderBar = (props) => {
  const navigate = useNavigate();
  const [currentUser, setCurrentUser] = useState(true);
  const clickLogout = useCallback(() => {
    // accountStore.clearStore();
    // companyStore.clearStore();
    // userStore.clearStore();
    // notificationStore.clearStore();
    // authenticationStore.userLogout();
    // navigate("/");
    setCurrentUser(false);
  }, []);

  const items = [
    {
      key: "1",
      label: (
        <div
          style={{ color: "blue", cursor: "pointer" }}
          onClick={() => navigate("/profile")}
        >
          Trang chủ
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          style={{ color: "red", cursor: "pointer" }}
          onClick={() => clickLogout()}
        >
          <LogoutOutlined style={{ marginRight: "5px" }} />
          Đăng Xuất
        </div>
      ),
      danger: true,
    },
  ];

  return (
    <HeaderContainer>
      <Logo href="/">
        <img src={logoImage} alt="Your Company" />
        <div>LOGIWARE</div>
      </Logo>
      {currentUser == true ? (
        <div className="flex items-center">
          <Link to={"/provider/warehouse"}>
            <ToDashBoard className="flex justify-center items-center">
              Truy cập trang quản lý
              <div style={{ paddingBottom: "2px" }}>&gt;&gt;</div>
            </ToDashBoard>
          </Link>
          <div className="flex">
            <Dropdown
              menu={{ items }}
              placement="topLeft"
              trigger={["click"]}
              className="sidebarItem"
            >
              <div className="flex items-center hover:cursor-pointer">
                <Avatar
                  size={28}
                  style={{ backgroundColor: green, fontSize: 12 }}
                >
                  {/* {currentUser && subStringAvatar(currentUser.username)} */}
                  {subStringAvatar("Nguyen Van A")}
                </Avatar>
                <span
                  style={{
                    margin: "0 7px",
                    fontWeight: 500,
                  }}
                >
                  Mr {utils.getNameInCapitalize("Nguyen Van A")}
                </span>
              </div>
            </Dropdown>
          </div>
        </div>
      ) : (
        <AuthButtons>
          <LoginButton href="/login">Đăng nhập</LoginButton>
          <RegisterButton href="/register">Đăng ký</RegisterButton>
        </AuthButtons>
      )}
    </HeaderContainer>
  );
};

MainHeaderBar.propTypes = {
  title: PropTypes.string,
};

export default MainHeaderBar;
