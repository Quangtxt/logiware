import React, {
  memo,
  useCallback,
  useEffect,
  useState,
  useContext,
} from "react";
import PropTypes from "prop-types";
// Component

// Styled Component

// Ant design
import { Avatar, Badge, Dropdown, Menu, message, Tabs, Tooltip } from "antd";
import {
  FacebookFilled,
  InstagramFilled,
  PhoneFilled,
  MailFilled,
} from "@ant-design/icons";

// Other
import { inject, observer } from "mobx-react";
import { blue } from "../../color";
import logoImage from "../../assets/logo.jpg";
// import { subStringAvatar } from "../Common/CellText";

import { StoreContext } from "../../stores/storeContext";

import {
  HeaderContainer,
  HeaderContent,
  Logo,
  LanguageSelector,
  AuthButtons,
  LoginButton,
  RegisterButton,
  MobileMenuButton,
} from "./MainFooterBarStyled";

const { TabPane } = Tabs;

const MainFooterBar = (props) => {
  return (
    <footer className="bg-neutral-900 shadow">
      <div className="py-6 px-2 md:px-6 md:flex md:items-center md:justify-between ">
        {" "}
        <div className="grid grid-cols-1 md:grid-cols-2 text-neutral-50 gap-6 px-3 md:px-0">
          <div className="flex flex-col gap-3 font-jakarta justify-start md:justify-center">
            <a href="" className="flex"></a>
            <p className="text-base">
              Nền Tảng Logistics Đa Dịch Vụ
              <br />
              <span>Đầu Tiên tại Việt Nam</span>
            </p>
            <p className="text-14">
              Nền tảng công nghệ kết nối các nhà cung cấp dịch vụ hậu cần đáng
              tin cậy với doanh nghiệp đa dạng quy mô trên khắp Việt Nam, mang
              đến sự minh bạch, linh hoạt và tiết kiệm chi phí.
            </p>
          </div>
          <div
            className=" grid grid-cols-1 md:grid-cols-2 gap-6"
            style={{ gridTemplateColumns: "repeat(2,auto)" }}
          >
            <div className="flex flex-col gap-1 text-14">
              <div>
                <p className="text-md font-bold tracking-wide mb-2">
                  Thông tin liên lạc
                </p>
              </div>
              <p>
                Số 35 Đường B4, KĐT Sala, Phường. An Lợi Đông, Thành Phố Thủ Đức
                (Quận 2), TP.HCM
              </p>
              <p className="inline-flex gap-3">
                <MailFilled />
                info@logiwware.io
              </p>
              <p className="inline-flex gap-3">
                <PhoneFilled />
                +84 28 6270 7628
              </p>
            </div>
            <div className="flex justify-start">
              <div className="flex flex-col gap-1 text-14 col-span-2 md:col-span-1 md:pr-12">
                <div>
                  <p className="text-md font-bold tracking-wide mb-2">
                    Công ty
                  </p>
                </div>
                <a
                  className="hover:underline"
                  target="_blank"
                  rel="noreferrer"
                  href="/"
                >
                  {" "}
                  Quy chế hoạt động
                </a>
                <a
                  className="hover:underline"
                  target="_blank"
                  rel="noreferrer"
                  href="/"
                >
                  Giải quyết khiếu nại
                </a>
                <a
                  className="hover:underline"
                  target="_blank"
                  rel="noreferrer"
                  href="/"
                >
                  Chính sách bảo mật
                </a>
                <a className="hover:underline" href="/">
                  Nhà kho
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full bg-neutral-600  p-4 shadow md:p-6 md:h-16">
        <div className="md:px-2 flex  justify-between items-center">
          <span className="text-sm text-neutral-50 sm:text-center ">
            © 2
            <a
              target="_blank"
              rel="noreferrer"
              className="hover:underline ml-1"
              href="https://wareflex.io"
            >
              LOGIWARE | v1.1.0
            </a>
          </span>
          <div className="ml-auto flex items-center justify-end gap-4 text-neutral-50">
            <span className="hidden md:block text-3 pointer-events-none mr-2 hover:underline">
              Follow us
            </span>
            <a
              target="_blank"
              rel="noreferrer"
              className="mr-1 text-3 hover:underline"
              href=""
            >
              <FacebookFilled />
            </a>
            <a
              target="_blank"
              rel="noreferrer"
              className="mr-1 text-3 hover:underline"
              href=""
            >
              <InstagramFilled />
            </a>
            <a className="text-3 hover:underline" href="tel:+84 ">
              <PhoneFilled />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default MainFooterBar;
