import React, { useState } from "react";
import { Carousel, Row, Col } from "antd";
import kho1Image from "../../assets/kho1.jpg";
import heroImage from "../../assets/hero.png";

import { BackGround, Introduction } from "./SlideBarStyled";
const SlideBar = () => {
  const [images, setImages] = useState([heroImage, heroImage]);
  return (
    <Carousel autoplay arrows>
      {images.map((image, index) => (
        <BackGround key={index} background_image={image}>
          <Row style={{ marginLeft: -8, marginRight: -8, rowGap: 16 }}>
            <Col xs={24}>
              <Introduction>
                Nền Tảng Logistics Đa Dịch Vụ <br />
                <span>Đầu Tiên tại Việt Nam</span>
              </Introduction>
            </Col>
          </Row>
        </BackGround>
      ))}
    </Carousel>
  );
};

export default SlideBar;
