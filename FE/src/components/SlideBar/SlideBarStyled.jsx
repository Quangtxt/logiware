import styled, { css } from "styled-components";

export const BackGround = styled.div`
  min-height: 500px;
  margin: 0;
  padding: 32px;
  background-size: cover;
  background-image: linear-gradient(
      0deg,
      rgba(1, 44, 21, 0.4),
      rgba(1, 44, 21, 0.4)
    ),
    url(${({ background_image }) => background_image});

  abs {
    max-width: 1200px;
    margin: auto;
  }
  .tabs .ant-tabs-tab {
    border: none !important;
    width: 320px;
    height: 56px;
    transition: none !important;
  }

  .tabs .ant-tabs-nav {
    margin-bottom: 0;
  }

  .tabs .ant-tabs-nav:before {
    border-bottom: none;
  }

  .tabs .ant-tabs-tab-btn {
    color: #fff;
    font-size: 20px;
    font-weight: 700;
    width: 100%;
    text-align: center;
    transition: none !important;
  }

  .tabs .ant-tabs-tab-active {
    background: hsla(0, 7%, 8%, 0.9) !important;
  }

  .tabs .ant-tabs-content-holder {
    background: hsla(0, 7%, 8%, 0.9);
    height: -moz-fit-content;
    height: fit-content;
    border-radius: 0 8px 8px 8px;
  }
  .tabs .ant-tabs-nav .ant-tabs-ink-bar {
    visibility: hidden;
  }
  @media only screen and (max-width: 480px) {
    .tabs .ant-tabs-tab {
      width: -moz-fit-content;
      width: fit-content;
      height: 48px;
      padding: 8px !important;
    }

    .tabs .ant-tabs-tab-btn {
      font-size: 13px;
      font-weight: 500;
      width: 100%;
    }
  }
`;
export const Introduction = styled.h1`
  font-size: 60px;
  font-weight: 700;
  color: #fff;
  margin: auto auto 24px;
  padding-top: 40px;
  text-align: center;
  span {
    color: #00e73e;
  }
`;
