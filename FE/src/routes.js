import HomePage from "./pages/HomePage/HomePage";
import ProductDetailPage from "./pages/ProductDetailPage/ProductDetailPage";
import P_CompanyPage from "./pages/Provider/P_CompanyPage/P_CompanyPage";
import P_CreateWarehousesPage from "./pages/Provider/P_CreateWarehousesPage";
import P_WarehousesPage from "./pages/Provider/P_WarehousesPage/P_WarehousesPage";

export const normalRoutes = [
  {
    path: "/login",
    component: HomePage,
    name: "Login",
  },
  {
    path: "/",
    component: HomePage,
    name: "Trang chủ",
  },
  {
    path: "/home",
    component: HomePage,
    name: "HomePage",
  },
  {
    path: "/warehouse/:id",
    component: ProductDetailPage,
    name: "ProductDetailPage",
  },
  {
    path: "/provider/company",
    component: P_CompanyPage,
    name: "Công ty",
  },
  {
    path: "/provider/warehouse",
    component: P_WarehousesPage,
    name: "Kho dịch vụ",
  },
  {
    path: "/provider/warehouse/add-warehouse",
    component: P_CreateWarehousesPage,
    name: "Tạo kho",
  },
];

export const routes = [];
