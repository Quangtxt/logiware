import React, { memo, useEffect, useState } from "react";

import UserLayout from "../../layouts/UserLayout/UserLayout";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import WarehousesPage from "../../pages/WarehousesPage/WarehousesPage";

const HomePage = (props) => {
  return (
    <UserLayout showFooter={true} showSlide={true}>
      <ContentBlockWrapper>
        <WarehousesPage />
      </ContentBlockWrapper>
    </UserLayout>
  );
};

export default HomePage;
