import React, { memo, useEffect, useState } from "react";

import { Tabs, Card, Button, List, Avatar, Divider, Row, Col } from "antd";
import { EditOutlined } from "@ant-design/icons";
import { CompanyContainer } from "./P_CompanyPageStyled";
import ProviderLayout from "../../../layouts/ProviderLayout/ProviderLayout";

const { TabPane } = Tabs;

const company = {
  name: "logite",
  email: "q@gmail.com",
  phone: "-",
  website: "-",
  representative: {
    name: "Praveen Kumar Sirisolla",
    email: "temp@gmail.com",
    phone: "0867 024 209",
  },
  companyInfo: {
    name: "logite",
    taxCode: "hhhh",
    address: "ha noi",
    shortName: "-",
    legalRepresentative: "-",
    position: "-",
  },
  legalDocuments: {
    businessLicense: "-",
  },
  users: [
    {
      status: "Kích hoạt",
      email: "quangtxt2k2@gmail.com",
      role: "Quản trị viên",
      phone: "0866 405 629",
      date: "25/06/2024",
    },
  ],
};

const P_CompanyPage = (props) => {
  return (
    <>
      <ProviderLayout>
        <CompanyContainer>
          <span className="title">Hồ sơ doanh nghiệp</span>
          <div className="content">
            <div className="content_left">
              <div className="flex flex-col gap-16 overflow-y-auto max-h-full">
                <div className="flex flex-col gap-16 items-center">
                  <h3 className="font-bold text-center">{company.name}</h3>
                </div>
                <Divider className="custom-divider" />
                <div className="flex flex-col gap-4">
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Email:</span>
                    <span className="text-sm text-neutral-900">
                      {company.email}
                    </span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Phone:</span>
                    <span className="text-sm text-neutral-900">
                      {company.phone}
                    </span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Website:</span>
                    <span className="text-sm text-neutral-900">
                      {company.website}
                    </span>
                  </div>
                </div>
                <Divider className="custom-divider" />
                <div className="flex flex-col gap-4">
                  <h4 className="text-neutral-400">Liên hệ chính</h4>
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Tên:</span>
                    <span className="text-sm text-neutral-900">
                      {company.representative.name}
                    </span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Email:</span>
                    <span className="text-sm text-neutral-900">
                      {company.representative.email}
                    </span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-xs text-neutral-400">Phone:</span>
                    <span className="text-sm text-neutral-900">
                      {company.representative.phone}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="content_right">
              <Tabs defaultActiveKey="1" className="px-4">
                <TabPane tab="Chi tiết" key="1">
                  <div className="flex flex-col gap-16">
                    <div className="flex flex-col">
                      <h4 style={{ color: "#264fb4" }}>THÔNG TIN CÔNG TY</h4>
                      <Row>
                        <Col xs={24}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Tên công ty:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.name}
                            </span>
                          </div>
                        </Col>
                        <Col xs={12}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Mã số thuế:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.taxCode}
                            </span>
                          </div>
                        </Col>
                        <Col xs={12}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Địa chỉ:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.address}
                            </span>
                          </div>
                        </Col>
                        <Col xs={24}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Tên ngắn gọn:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.shortName}
                            </span>
                          </div>
                        </Col>
                        <Col xs={12}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Người đại diện theo pháp luật:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.legalRepresentative}
                            </span>
                          </div>
                        </Col>
                        <Col xs={12}>
                          <div className="flex flex-col">
                            <span className="text-xs text-neutral-400">
                              Chức vụ:
                            </span>
                            <span className="text-sm text-neutral-900">
                              {company.companyInfo.position}
                            </span>
                          </div>
                        </Col>
                      </Row>
                    </div>
                    <div className="flex flex-col mb-4">
                      <div className="flex flex-col gap-4">
                        <h4 style={{ color: "#264fb4" }}>VĂN BẢN PHÁP LÝ</h4>
                        <div className="flex flex-col">
                          <span className="text-xs text-neutral-400">
                            Giấy phép kinh doanh:
                          </span>
                          <span className="text-sm text-neutral-900">
                            {company.legalDocuments.businessLicense}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </TabPane>
                <TabPane tab="Người dùng" key="2">
                  <Button type="primary" style={{ marginBottom: "20px" }}>
                    + Người dùng mới
                  </Button>
                  <List
                    itemLayout="horizontal"
                    dataSource={company.users}
                    renderItem={(user) => (
                      <List.Item>
                        <List.Item.Meta
                          avatar={<Avatar icon={<EditOutlined />} />}
                          title={<a href="mailto:{user.email}">{user.email}</a>}
                          description={
                            <div>
                              <p>{user.role}</p>
                              <p>{user.phone}</p>
                              <p>{user.date}</p>
                            </div>
                          }
                        />
                        <div>{user.status}</div>
                      </List.Item>
                    )}
                  />
                </TabPane>
              </Tabs>
            </div>
          </div>
        </CompanyContainer>
      </ProviderLayout>
    </>
  );
};

export default P_CompanyPage;
