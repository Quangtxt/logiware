import { Card, Col, List } from "antd";
import styled, { css } from "styled-components";

export const CompanyContainer = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  gap: 8px;
  .title {
    height: 40px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 8px;
    font-size: 24px;
    line-height: 32px;
    font-weight: 600;
    color: rgb(40, 43, 50);
  }
  .content {
    height: 100%;
    max-height: calc(100% - 48px);
    background: #fff;
    border-radius: 8px;
    box-shadow:
      0 2px 8px 0 rgba(0, 0, 0, 0.1),
      0 1px 2px -1px rgba(0, 0, 0, 0.1);
    display: flex;
  }
  .content_left {
    width: 392px;
    min-width: 392px;
    border-right: 1px solid #e2e8f0;
    padding: 24px;
  }
  .content_right {
    height: 100%;
    width: 100%;
    padding: 0;
  }
  .custom-divider {
    margin: 10px 0 !important;
  }
`;
