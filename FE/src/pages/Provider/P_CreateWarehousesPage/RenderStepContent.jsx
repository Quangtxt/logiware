import { Input, Row, Form, Select, Col } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import React, { memo, useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";

import Step1 from "./Steps/Step1";
import Step2 from "./Steps/Step2";
import Step3 from "./Steps/Step3";
import Step4 from "./Steps/Step4";
const RenderStepContent = (props) => {
  const { current } = props;

  //gg map

  return (
    <Form layout="vertical">
      {current == 0 && <Step1></Step1>}
      {current == 1 && <Step2></Step2>}
      {current == 2 && <Step3></Step3>}
      {current == 3 && <Step4></Step4>}
    </Form>
  );
};

export default RenderStepContent;
