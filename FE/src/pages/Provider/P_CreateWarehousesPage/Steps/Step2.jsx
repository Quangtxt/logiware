import { Input, Row, Form, Select, Col, Table, Switch } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import React, { memo, useCallback, useState } from "react";
import { StepContainer } from "../P_CreateWarehousesPageStyled";
import { GoogleMap, useJsApiLoader } from "@react-google-maps/api";

const { Column } = Table;

const Step2 = (props) => {
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: import.meta.env.VITE_Google_API_Key,
  });

  const [map, setMap] = useState(null);

  const onLoad = useCallback(function callback(map) {
    // This is just an example of getting and using the map instance!!! don't just blindly copy!
    const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds);

    setMap(map);
  }, []);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);
  const containerStyle = {
    width: "400px",
    height: "400px",
  };

  const center = {
    lat: -3.745,
    lng: -38.523,
  };

  //phi
  const data = [
    {
      key: "1",
      uom: "* SQM (m2)",
      dailyPrice: 0,
      monthlyPrice: 0,
      platform: true,
    },
    {
      key: "2",
      uom: "Pallet",
      dailyPrice: 0,
      monthlyPrice: 0,
      platform: false,
    },
    {
      key: "3",
      uom: "CBM (m3)",
      dailyPrice: 0,
      monthlyPrice: 0,
      platform: false,
    },
    {
      key: "4",
      uom: "Trọng lượng (Tons)",
      dailyPrice: 0,
      monthlyPrice: 0,
      platform: false,
    },
  ];
  return (
    <>
      <StepContainer>
        <Row
          style={{ height: "calc(100% - 60px)" }}
          className="overflow-y-auto p-8"
        >
          <Col xs={8} className="pl-2 pr-2">
            <div className="step_title">Địa điểm</div>

            <Form.Item name="address_provinceId" label="Tỉnh/thành">
              <Select
                placeholder="Vui lòng chọn"
                optionFilterProp="label"
                showSearch
                onChange={onChange}
                onSearch={onSearch}
                options={[]}
              />
            </Form.Item>
            <Form.Item name="address_provinceId2" label="Quận">
              <Select
                placeholder="Vui lòng chọn"
                optionFilterProp="label"
                showSearch
                onChange={onChange}
                onSearch={onSearch}
                options={[]}
              />
            </Form.Item>
            <Form.Item name="address_provinceId3" label="Phường">
              <Select
                placeholder="Vui lòng chọn"
                optionFilterProp="label"
                showSearch
                onChange={onChange}
                onSearch={onSearch}
                options={[]}
              />
            </Form.Item>
            <Form.Item name="address_provinceId4" label="Đường">
              <Input placeholder="Nhập địa chỉ"></Input>
            </Form.Item>
            <Form.Item name="address_provinceId5" label="Địa chỉ chi tiết">
              <Input placeholder="Nhập "></Input>
            </Form.Item>
            <Form.Item name="cotact" label="Liên hệ">
              <Select optionFilterProp="label" options={[]} />
            </Form.Item>
          </Col>
          <Col xs={16} className="pl-2 pr-2">
            {/* <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={10}
          onLoad={onLoad}
          onUnmount={onUnmount}
        >
          <></>
        </GoogleMap> */}
            day la gg map
          </Col>
        </Row>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Sức chứa</div>
        <div className="ml-2 mr-2">
          <Row gutter={[16, 0]}>
            <Col xs={12}>
              <Form.Item name="1" label="Tổng diện tích đất (m²)">
                <Input placeholder="Nhập" type="number"></Input>
              </Form.Item>
            </Col>
            <Col xs={12}>
              <Form.Item name="1" label="Diện tích xây dựng kho (m²)">
                <Input placeholder="Nhập" type="number"></Input>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 0]}>
            <Col xs={16}>
              <Form.Item name="1" label="Sức chứa kho">
                <Input placeholder="Nhập "></Input>
              </Form.Item>
            </Col>
            <Col xs={8}>
              <Form.Item name="1" label=" ">
                <Select placeholder="Đơn vị tính"></Select>
              </Form.Item>
            </Col>
          </Row>
          <div className="flex gap-8">
            <div className="flex gap-2">
              <Form.Item name="1" label="Tổng công suất inbound/ngày">
                <Input placeholder="Nhập "></Input>
              </Form.Item>
              <Form.Item name="1" label=" ">
                <Select placeholder="Đơn vị tính"></Select>
              </Form.Item>
            </div>
            <div className="flex gap-2">
              <Form.Item name="1" label="Tổng công suất outbound/ngày">
                <Input placeholder="Nhập "></Input>
              </Form.Item>
              <Form.Item name="1" label=" ">
                <Select placeholder="Đơn vị tính"></Select>
              </Form.Item>
            </div>
          </div>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Phí lưu trữ</div>
        <div className="ml-2 mr-2">
          <Table dataSource={data} pagination={false}>
            <Column title="#" dataIndex="key" key="key" />
            <Column title="UOM" dataIndex="uom" key="uom" />
            <Column
              title="Giá theo ngày"
              dataIndex="dailyPrice"
              key="dailyPrice"
              render={(text, record) => (
                <Input defaultValue={text} type="number" />
              )}
            />
            <Column
              title="Giá theo tháng"
              dataIndex="monthlyPrice"
              key="monthlyPrice"
              render={(text, record) => (
                <Input defaultValue={text} type="number" />
              )}
            />
            <Column
              title="Hiển thị trên Platform"
              dataIndex="platform"
              key="platform"
              render={(text, record) => <Switch defaultChecked={text} />}
            />
          </Table>
        </div>
      </StepContainer>
    </>
  );
};

export default Step2;
