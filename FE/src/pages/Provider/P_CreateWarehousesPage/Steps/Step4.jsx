import React from "react";
import { Form, Row, Col, Input, Select, Upload } from "antd";

import { StepContainer } from "../P_CreateWarehousesPageStyled";
const { Option } = Select;
const { Dragger } = Upload;
const Step4 = (props) => {
  const fileProps = {
    name: "file",
    multiple: true,
    accept: ".jpeg,.png,.pdf",
    maxCount: 1,
    beforeUpload: (file) => {
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error("File phải nhỏ hơn 2MB!");
      }
      return isLt2M;
    },
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} tải lên thành công.`);
      } else if (status === "error") {
        message.error(`${info.file.name} tải lên thất bại.`);
      }
    },
  };
  return (
    <>
      <StepContainer>
        <div className="step_title">Đặc trưng</div>
        <div className="px-4 mb-3">
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Phương pháp bảo quản" name="storageMethod">
                <Select placeholder="Vui lòng chọn">
                  <Option value="method1">Phương pháp 1</Option>
                  <Option value="method2">Phương pháp 2</Option>
                  <Option value="method3">Phương pháp 3</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="An toàn và bảo mật" name="security">
                <Select placeholder="Vui lòng chọn">
                  <Option value="secure1">Bảo mật 1</Option>
                  <Option value="secure2">Bảo mật 2</Option>
                  <Option value="secure3">Bảo mật 3</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                label="Thiết bị điện tử và tự động hoá phần cứng"
                name="electronics"
              >
                <Select placeholder="Vui lòng chọn">
                  <Option value="device1">Thiết bị 1</Option>
                  <Option value="device2">Thiết bị 2</Option>
                  <Option value="device3">Thiết bị 3</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Tiện nghi" name="amenities">
                <Select placeholder="Vui lòng chọn">
                  <Option value="amenity1">Tiện nghi 1</Option>
                  <Option value="amenity2">Tiện nghi 2</Option>
                  <Option value="amenity3">Tiện nghi 3</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                label="Tiết kiệm năng lượng, thân thiện môi trường"
                name="energySaving"
              >
                <Select placeholder="Vui lòng chọn">
                  <Option value="option1">Lựa chọn 1</Option>
                  <Option value="option2">Lựa chọn 2</Option>
                  <Option value="option3">Lựa chọn 3</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item label="Các chứng nhận">
                <Dragger {...fileProps}>
                  <ul
                    style={{ listStyleType: "disc" }}
                    className="text-left ul_file"
                  >
                    <li>Các định dạng file được chấp nhận : jpeg/ png/ pdf</li>
                    <li>Định dang hình ảnh : Chiều ngang</li>
                    <li>Kích thước file tối đa : 2MB</li>
                  </ul>
                </Dragger>
              </Form.Item>
            </Col>
          </Row>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Thông tin thêm</div>
        <div className="px-4 mb-3">
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Chiều cao (m)" name="height">
                <Input placeholder="Nhập" type="number" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Kệ hàng" name="shelves">
                <Select placeholder="Nhập" options={[]} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Kích thước xe tối đa" name="maxCarSize">
                <Select placeholder="Nhập">
                  <Option value="small">Nhỏ</Option>
                  <Option value="medium">Vừa</Option>
                  <Option value="large">Lớn</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Số lượng cổng" name="gateCount">
                <Input placeholder="Nhập" type="number" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Loại bố trí kho" name="warehouseType">
                <Select placeholder="Nhập">
                  <Option value="type1">Loại 1</Option>
                  <Option value="type2">Loại 2</Option>
                  <Option value="type3">Loại 3</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Số lượng nhân viên hoạt động trung bình"
                name="averageEmployeeCount"
              >
                <Input placeholder="Nhập" type="number" />
              </Form.Item>
            </Col>
          </Row>
        </div>
      </StepContainer>
    </>
  );
};

export default Step4;
