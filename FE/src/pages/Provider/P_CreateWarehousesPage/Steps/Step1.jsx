import {
  Input,
  Row,
  Form,
  Select,
  Col,
  Radio,
  Upload,
  Switch,
  TimePicker,
  Checkbox,
} from "antd";
import dayjs from "dayjs";
import React, { memo, useCallback, useState } from "react";
import { SelectType, StepContainer } from "../P_CreateWarehousesPageStyled";

const { TextArea } = Input;
const { Dragger } = Upload;

const Step1 = (props) => {
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const fileProps = {
    name: "file",
    multiple: true,
    accept: ".jpeg,.png,.pdf",
    maxCount: 5,
    fileList: [],
    beforeUpload: (file) => {
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error("File phải nhỏ hơn 2MB!");
      }
      return isLt2M;
    },
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} tải lên thành công.`);
      } else if (status === "error") {
        message.error(`${info.file.name} tải lên thất bại.`);
      }
    },
  };
  return (
    <>
      <StepContainer>
        <div className="step_title">TỔNG QUAN</div>
        <div className="ml-2 mr-2">
          <Form.Item
            name="name"
            label="Tên"
            rules={[
              {
                // required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="type_ware"
            label="Loại kho"
            rules={[
              {
                // required: true,
              },
            ]}
          >
            <Select
              optionFilterProp="label"
              placeholder="Vui lòng chọn"
              onChange={onChange}
              options={[
                {
                  value: "1",
                  label: "Bãi ngoài trời",
                },
                {
                  value: "2",
                  label: "Kho thường",
                },
                {
                  value: "3",
                  label: "Kho kiểm soát nhiệt độ",
                },
              ]}
            />
          </Form.Item>
          <Form.Item name="description_bonded" label="Kho ngoại quan">
            <Radio.Group value={1}>
              <Radio value={1}>Có</Radio>
              <Radio value={2}>Không</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            name="description_Description"
            label="Ghi chú"
            rules={[
              {
                // required: true,
              },
            ]}
          >
            <TextArea style={{ height: "60px" }} />
          </Form.Item>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Ảnh</div>
        <div className="ml-2 mr-2">
          <Form.Item name="images">
            <Dragger {...fileProps}>
              <ul
                style={{ listStyleType: "disc" }}
                className="text-left ul_file"
              >
                <li>Các định dạng file được chấp nhận : jpeg/ png</li>
                <li>Định dang hình ảnh : Chiều ngang</li>
                <li>Kích thước file tối đa : 2MB</li>
                <li>Ít nhất 5 file</li>
              </ul>
            </Dragger>
          </Form.Item>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Hoạt động</div>
        <div className="ml-2 mr-2">
          <Form.Item name="2">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Hai</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="3">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Ba</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="4">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Tư</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="5">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Năm</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="6">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Sáu</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="7">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Thứ Bảy</span>
              <Switch defaultChecked />
              <span>Mở</span>
              <TimePicker defaultValue={dayjs("08:00", "HH:mm")} />
              <TimePicker defaultValue={dayjs("17:00", "HH:mm")} />
            </div>
          </Form.Item>
          <Form.Item name="8">
            <div className="flex items-center gap-6">
              <span style={{ width: "100px" }}>Chủ Nhật</span>
              <Switch />
            </div>
          </Form.Item>
          <Form.Item name="isCheckbox" valuePropName="checked">
            <Checkbox>Làm việc xuyên ngày nghỉ</Checkbox>
          </Form.Item>
        </div>
      </StepContainer>
    </>
  );
};

export default Step1;
