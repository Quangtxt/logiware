import { Form, Row, Col, Input, Radio, Tree, Checkbox } from "antd";
import React from "react";
import { StepContainer } from "../P_CreateWarehousesPageStyled";

const { TextArea } = Input;
const Step3 = (props) => {
  //danh muc
  const treeData = [
    {
      title: "Sản phẩm công nghiệp",
      key: "0-0",
      children: [
        { title: "Đồ gia dụng", key: "0-0-0" },
        { title: "Hàng điện máy, điện lạnh", key: "0-0-1" },
        { title: "Giấy, sách báo, văn phòng phẩm", key: "0-0-2" },
        { title: "Bao bì, vật liệu quảng cáo", key: "0-0-3" },
        { title: "Phụ tùng & công cụ công nghiệp", key: "0-0-4" },
        { title: "Thiết bị, linh kiện điện tử", key: "0-0-5" },
        { title: "Thiết bị, linh kiện điện", key: "0-0-6" },
        { title: "Máy móc & dây chuyền sản xuất", key: "0-0-7" },
        { title: "Xe cộ, máy cơ giới", key: "0-0-8" },
      ],
    },
    {
      title: "Dược phẩm",
      key: "0-1",
      children: [
        { title: "Đồ gia dụng", key: "0-1-0" },
        { title: "Hàng điện máy, điện lạnh", key: "0-1-1" },
        { title: "Giấy, sách báo, văn phòng phẩm", key: "0-1-2" },
        { title: "Bao bì, vật liệu quảng cáo", key: "0-1-3" },
        { title: "Phụ tùng & công cụ công nghiệp", key: "0-1-4" },
        { title: "Thiết bị, linh kiện điện tử", key: "0-1-5" },
        { title: "Thiết bị, linh kiện điện", key: "0-1-6" },
        { title: "Máy móc & dây chuyền sản xuất", key: "0-1-7" },
        { title: "Xe cộ, máy cơ giới", key: "0-1-8" },
      ],
    },
    {
      title: "Vật liệu xây dựng",
      key: "0-2",
      children: [
        { title: "Đồ gia dụng", key: "0-2-0" },
        { title: "Hàng điện máy, điện lạnh", key: "0-2-1" },
        { title: "Giấy, sách báo, văn phòng phẩm", key: "0-2-2" },
        { title: "Bao bì, vật liệu quảng cáo", key: "0-2-3" },
        { title: "Phụ tùng & công cụ công nghiệp", key: "0-2-4" },
        { title: "Thiết bị, linh kiện điện tử", key: "0-2-5" },
        { title: "Thiết bị, linh kiện điện", key: "0-2-6" },
        { title: "Máy móc & dây chuyền sản xuất", key: "0-2-7" },
        { title: "Xe cộ, máy cơ giới", key: "0-2-8" },
      ],
    },
    {
      title: "Nguyên vật liệu công nghiệp",
      key: "0-3",
    },
    {
      title: "Hàng bán lẻ & Thương mại điện tử",
      key: "0-4",
    },
    {
      title: "Hàng thực phẩm đóng gói và đồ uống",
      key: "0-5",
    },
  ];
  //dich vu
  const options = [
    "Dịch vụ CFS",
    "Thẩm định chất lượng bên thứ ba",
    "Xử lý độ ẩm",
    "Dịch vụ vận tải",
    "Đóng kiện lại",
    "Cross docking",
    "Nhật hàng",
    "Bọc màng co pallet",
    "Kiểm Đếm Bàn Giao",
    "Dịch vụ EPE",
    "Vách ngăn di động",
    "Tái chế",
    "Dịch vụ hải quan",
    "Gắn thẻ",
    "Lưu kho bằng pallet",
    "Cắt hàng",
    "Đóng pallet",
    "Dịch vụ Fulfillment",
    "Kiểm tra chất lượng hàng",
    "Hun trùng",
    "Đại lý tàu biển",
    "Lắp ráp",
    "Dán nhãn",
    "Kiểm kê",
    "Đóng gói",
    "Bốc xếp",
  ];
  const optionsCSHT = [
    "Cân xe tải / Cầu cân",
    "Kệ hàng",
    "Sàn nâng tự động",
    "CCTV",
    "Đầu phun PCCC",
    "Cân xe tải",
    "Phễu",
    "Băng chuyền",
    "PCCC cơ bản",
  ];
  return (
    <>
      <StepContainer>
        <div className="step_title">DANH MỤC SẢN PHẨM</div>
        <div className="px-4 mb-3">
          <Form.Item>
            <Tree checkable treeData={treeData} />
          </Form.Item>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Dịch vụ</div>
        <div className="px-4 mb-3">
          <Form.Item>
            <Row gutter={[16, 16]}>
              {options.map((option, index) => (
                <Col span={8} key={index}>
                  <Checkbox>{option}</Checkbox>
                </Col>
              ))}
            </Row>
          </Form.Item>
        </div>
      </StepContainer>
      <StepContainer>
        <div className="step_title">Cơ sở hạ tầng</div>
        <div className="px-4 mb-3">
          <Form.Item>
            <Row gutter={[16, 16]}>
              {optionsCSHT.map((option, index) => (
                <Col span={12} key={index}>
                  <Checkbox>{option}</Checkbox>
                </Col>
              ))}
            </Row>
          </Form.Item>
        </div>
      </StepContainer>
    </>
  );
};

export default Step3;
