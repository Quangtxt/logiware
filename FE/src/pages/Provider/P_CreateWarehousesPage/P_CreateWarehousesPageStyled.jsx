import { Card, Col, List } from "antd";
import styled, { css } from "styled-components";
export const AddWarehouse = styled.div`
  border-radius: 8px;
  max-height: calc(100vh - 6rem);
  height: calc(100vh - 6rem);
  display: flex;
  .step_create {
    width: 230px;
    background: #f1f5f9;
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
    padding: 16px;
    height: 100%;
    overflow-y: auto;
    margin-right: 20px;
  }
  .step_create_content {
    width: 100%;
    height: 100%;
    max-height: 100%;
    position: relative;
    border-top-right-radius: 8px;
    max-width: calc(100% - 300px);
    overflow-x: auto;
  }
  .step_content {
    justify-content: center;
    height: calc(100% - 60px);
    overflow-y: auto;
    padding-top: 16px;
    display: flex;
    align-items: center;
  }
  .next_step {
    height: 60px;
    padding: 0 16px;
    width: 100%;
    position: sticky;
    bottom: 0;
    flex-direction: row;
    gap: 12px;
    justify-content: space-between;
    border-top: 1px solid #cbd5e1;
    z-index: 3;
    border-bottom-right-radius: 8px;
  }
`;
export const SelectType = styled.div`
  flex-direction: row;
  width: 360px;
  height: 68px;
  border: 1px solid #cbd5e1;
  border-radius: 4px;
  cursor: pointer;
`;
export const StepContainer = styled.div`
  border: 1px solid #dce0e5;
  border-radius: 8px;
  margin: 20px 0 20px 0;
  .step_title {
    padding: 12px 16px;
    color: #264fb4;
    font-size: 16px;
    line-height: 24px;
    font-weight: 700;
    box-sizing: border-box;
  }
  div[class*="ant-upload-drag"]
    > span[class*="ant-upload"]
    > div[class*="ant-upload-drag-container"] {
    display: flex;
    flex-direction: column;
    justify-content: center !important;
    align-items: center !important;
    gap: 12px;
  }
  .ul_file > li {
    font-size: 14px !important;
    color: var(--neutral-color);
    text-align: left;
    word-break: break-all;
  }
`;
