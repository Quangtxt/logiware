import { Input, Row, Button, message, Steps, theme } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import React, { memo, useEffect, useState } from "react";

import { AddWarehouse } from "./P_CreateWarehousesPageStyled";
import ProviderLayout from "../../../layouts/ProviderLayout/ProviderLayout";
import RenderStepContent from "./RenderStepContent";
import { useNavigate } from "react-router-dom";

const { Search } = Input;
const P_CreateWarehousesPage = (props) => {
  const navigate = useNavigate();
  const { token } = theme.useToken();
  const [current, setCurrent] = useState(0);
  const next = () => {
    setCurrent(current + 1);
  };
  const prev = () => {
    setCurrent(current - 1);
  };
  const saveDraft = () => {
    message.success("Đã lưu bản nháp");
    navigate("/provider/warehouse");
  };
  const steps = [
    {
      title: "Tổng quan - ảnh - hoạt động",
    },
    {
      title: "Địa điểm - sức chứa - liên hệ",
    },

    {
      title: "Các danh mục sản phẩm - dịch vụ - cơ sở hạ tầng",
    },
    {
      title: "Đặc trưng - Thông tin thêm",
    },
  ];
  const items = steps.map((item) => ({
    key: item.title,
    title: item.title,
  }));
  return (
    <ProviderLayout>
      <AddWarehouse className="bg-primary-background-content-color shadow-primary-background-color">
        <div className="step_create">
          <Steps current={current} items={items} direction="vertical" />
        </div>
        <div className="step_create_content ">
          <RenderStepContent current={current}></RenderStepContent>

          <div className="next_step bg-primary-background-content-color flex items-center">
            {current > 0 ? (
              <Button className="smallButton" onClick={() => prev()}>
                Trở lại
              </Button>
            ) : (
              <div></div>
            )}
            <span className="text-primary_1 color-neutral-800">
              <b>Bước {current + 1}</b> trên {steps.length}
            </span>
            {current < steps.length - 1 && (
              <div>
                {current != 0 && (
                  <Button className="smallButton" onClick={saveDraft}>
                    Lưu bản nháp
                  </Button>
                )}
                <Button type="primary" onClick={() => next()}>
                  Tiếp theo
                </Button>
              </div>
            )}
            {current === steps.length - 1 && (
              <Button
                type="primary"
                onClick={() => message.success("Processing complete!")}
              >
                Done
              </Button>
            )}
          </div>
        </div>
      </AddWarehouse>
    </ProviderLayout>
  );
};

export default P_CreateWarehousesPage;
