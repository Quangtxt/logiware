import { Input, Row, Col, Empty, Button, Table, Tag } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import React, { memo, useEffect, useState } from "react";

import { WareContent, OfProduce } from "./P_WarehousesPageStyled";
import UserLayout from "../../../layouts/UserLayout/UserLayout";
import ProviderLayout from "../../../layouts/ProviderLayout/ProviderLayout";
import ContentBlockWrapper from "../../../components/ContentBlockWrapper";
import { useNavigate } from "react-router-dom";

const { Search } = Input;
const P_WarehousesPage = (props) => {
  const navigate = useNavigate();
  const onSearch = (value, _e, info) => console.log(info?.source, value);
  const columns = [
    {
      title: "#",
      dataIndex: "index",
      key: "index",
      render: (text) => <span>{text}</span>,
    },
    {
      title: "Mã",
      dataIndex: "code",
      key: "code",
      render: (text) => (
        <div>
          <a href="">{text}</a>
          <br />
          <Tag color="blue">Đang xem xét</Tag>
        </div>
      ),
    },
    {
      title: "Loại",
      dataIndex: "type",
      key: "type",
    },
    {
      title: "Địa điểm",
      dataIndex: "location",
      key: "location",
    },
    {
      title: "Diện tích đất - Diện tích cơ sở",
      dataIndex: "area",
      key: "area",
    },
    {
      title: "Sức chứa khả dụng",
      dataIndex: "area2",
      key: "area2",
    },
    {
      title: "Liên hệ",
      dataIndex: "contact",
      key: "contact",
    },
    {
      title: "Lý do hủy",
      dataIndex: "cancel",
      key: "cancel",
    },
  ];

  const data = [
    {
      key: "1",
      index: "1",
      code: "WHS-4-00536",
      type: "Kho kiểm soát nhiệt độ",
      location: "Bà Rịa - Vũng Tàu",
      area: "10 m² - 1 m²",
      area2: "100%",
      contact: "Nguyễn Văn A",
      cancel: "",
    },
  ];

  return (
    <ProviderLayout>
      <ContentBlockWrapper>
        <WareContent className="flex flex-col height-full">
          <Row className="layoutWarehouse">
            <Col xs={24} md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  fontWeight: "700",
                  color: "#0a9830",
                }}
              >
                Nhà kho
              </h2>
            </Col>
            <Col xs={24} md={12}>
              <div className="flex justify-end gap-8">
                <Search placeholder="Tìm nhà kh0" onSearch={onSearch} />
                <Button>refresh</Button>
                <Button
                  onClick={() => navigate("/provider/warehouse/add-warehouse")}
                >
                  Add
                </Button>
              </div>
            </Col>
          </Row>
          {/* <OfProduce>
            <div className="item">
              <div className="item-title bg-custom">
                <div>
                  <span
                    style={{
                      color: " rgb(148, 163, 184)",
                      backgroundColor: "rgb(255, 255, 255)",
                      fontWeight: "600",
                    }}
                  >
                    Bản nháp
                  </span>
                </div>
                <div>
                  <span style={{ color: " rgb(148, 163, 184)" }}>0/0</span>
                </div>
              </div>
              <div className="item-content bg-custom">
                <Empty />
              </div>
            </div>
            <div className="item">
              <div className="item-title bg-custom">
                <div>
                  <span
                    style={{
                      color: " rgb(0, 0, 0)",
                      backgroundColor: "rgb(255, 255, 255)",
                      fontWeight: "600",
                    }}
                  >
                    Chờ xác nhận
                  </span>
                </div>
                <div>
                  <span style={{ color: " rgb(148, 163, 184)" }}>0/0</span>
                </div>
              </div>
              <div className="item-content bg-custom">
                <Empty />
              </div>
            </div>
            <div className="item">
              <div className="item-title bg-custom">
                <div>
                  <span
                    style={{
                      color: " rgb(255, 169, 64)",
                      backgroundColor: "rgb(255, 255, 255)",
                      fontWeight: "600",
                    }}
                  >
                    Chờ kiểm tra lại
                  </span>
                </div>
                <div>
                  <span style={{ color: " rgb(148, 163, 184)" }}>0/0</span>
                </div>
              </div>
              <div className="item-content bg-custom">
                <Empty />
              </div>
            </div>
            <div className="item">
              <div className="item-title bg-custom">
                <div>
                  <span
                    style={{
                      color: " rgb(16, 185, 129)",
                      backgroundColor: "rgb(255, 255, 255)",
                      fontWeight: "600",
                    }}
                  >
                    Xác nhận
                  </span>
                </div>
                <div>
                  <span style={{ color: " rgb(148, 163, 184)" }}>0/0</span>
                </div>
              </div>
              <div className="item-content bg-custom">
                <Empty />
              </div>
            </div>
          </OfProduce> */}
          <div className="table-container">
            <Table columns={columns} dataSource={data} pagination={false} />
          </div>
        </WareContent>
      </ContentBlockWrapper>
    </ProviderLayout>
  );
};

export default P_WarehousesPage;
