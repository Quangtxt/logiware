import { Card, Col, List } from "antd";
import styled, { css } from "styled-components";
export const WareContent = styled.div`
  .layoutWarehouse {
    width: 100%;
    flex: 1;
    align-items: center;
    margin-bottom: 16px;
    align-self: end;
    display: flex;
    max-height: 36px;
    max-width: 100%;
    align-self: center;
  }
  .table-container {
    overflow-x: auto;
    width: 100%;
  }

  .table-container table {
    width: max-content;
    min-width: 100%;
  }
`;
export const OfProduce = styled.div`
  height: 100%;
  max-width: 100%;
  padding-bottom: 8px;
  display: flex;
  gap: 16px;
  align-self: center;
  overflow-x: auto;
  overflow-y: hidden;
  .item {
    display: inline-block;
    overflow: hidden;
    padding-bottom: 8px;
    min-width: 275px;
    width: 275px;
  }
  .item-title {
    margin-bottom: 14px;
    display: flex;
    justify-content: space-between;
  }
  .item-content {
    height: calc(100% - 56px);
    overflow-y: auto;
    overflow-y: overlay;
    display: inline-block;
    width: 100%;
  }
  .bg-custom {
    background-color: #fff;
    border-radius: 8px;
    padding: 16px;
  }
`;
