import React, { memo, useEffect, useState } from "react";
import {
  Modal,
  Form,
  Input,
  Select,
  Checkbox,
  Row,
  Col,
  DatePicker,
} from "antd";
import dayjs from "dayjs";
import { CloseOutlined } from "@ant-design/icons";
import { ModalContent, TitleModal } from "./ProductDetailPageStyled";

const { TextArea } = Input;
// import heroImage from "../../assets/hero.png";

const PopupSendQuote = (props) => {
  //   const [images, setImages] = useState([heroImage, heroImage]);
  const { isModalOpen, handleCancel } = props;
  const handleOk = () => {
    console.log("ok");
  };
  const handleTypeWareChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const onChangeCheckBox = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  const onShowCancel = () => {
    Modal.confirm({
      title: `Bạn có chắc chắn muốn đóng không`,
      content: "Lưu ý: Dữ liệu sẽ không được lưu!",
      onOk: () => {
        handleCancel();
      },
      onCancel: () => {},
      okText: "Ok",
      cancelText: "Đóng",
    });
  };
  return (
    <Modal
      open={isModalOpen}
      onOk={handleOk}
      onCancel={onShowCancel}
      width="1000px"
      okText="Gửi đi"
    >
      <ModalContent className="overflow-x-hidden overflow-y-auto">
        <Form className="w-full">
          <div className="flex flex-col">
            <div className="flex flex-col gap-8">
              <TitleModal>
                1. Sản lượng lưu kho
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <div className="flex flex-row gap-8">
                <Form.Item>
                  <Input type="number" placeholder="0"></Input>
                </Form.Item>
                <Form.Item>
                  <Select
                    onChange={handleTypeWareChange}
                    placeholder="Chọn"
                    optionFilterProp="label"
                    showSearch
                    onSearch={onSearch}
                    dropdownStyle={{
                      minWidth: "170px",
                      overflow: "auto",
                    }}
                    options={[
                      { value: "1", label: "Trọng lượng (Tons)" },
                      { value: "2", label: "SQM (m2)" },
                      { value: "3", label: "Pallet" },
                      { value: "4", label: "CBM (m3)" },
                      { value: "5", label: "Container (TEU)" },
                    ]}
                  />
                </Form.Item>
              </div>
            </div>
            <div className="flex flex-col gap-8">
              <TitleModal>
                2. Danh mục sản phẩm
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <Form.Item>
                <Select
                  onChange={handleTypeWareChange}
                  placeholder="Chọn"
                  optionFilterProp="label"
                  showSearch
                  onSearch={onSearch}
                  options={[
                    { value: "1", label: "Rau, củ, quả tươi" },
                    { value: "2", label: "Thịt, cá, thủy hải sản" },
                    { value: "3", label: "Rau, củ , quả đã đóng gói" },
                    { value: "4", label: "Đồ uống không cồn" },
                    { value: "5", label: "Đồ uống có cồn" },
                  ]}
                />
              </Form.Item>
            </div>
            <div className="flex flex-col gap-8">
              <TitleModal>
                3. Sản lương dự kiến
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <div className="p-[12px] flex flex-col bg-[#f2f7ff]">
                <div className="text-green-600 text-base leading-6 font-bold">
                  Sản lượng nhập hàng
                </div>
                <Form.Item>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>Pallet</Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>
                      Carton, box, crate, bag
                    </Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>Container</Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>
                      Drum, oversized
                    </Checkbox>
                  </div>
                </Form.Item>
              </div>
              <div className="p-[12px] flex flex-col bg-[#f2f7ff]">
                <div className="text-green-600 text-base leading-6 font-bold">
                  Sản lượng xuất hàng
                </div>
                <Form.Item>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>Pallet</Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>
                      Carton, box, crate, bag
                    </Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>Container</Checkbox>
                  </div>
                  <div className="flex flex-col">
                    <Checkbox onChange={onChangeCheckBox}>
                      Drum, oversized
                    </Checkbox>
                  </div>
                </Form.Item>
              </div>
            </div>
            <div className="flex flex-col gap-8 ">
              <TitleModal>
                4. Thông tin dịch vụ
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <Form.Item>
                <Row>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox} checked>
                      Bốc xếp
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>
                      Kiểm đếm bàn giao
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox} checked>
                      Cất hàng
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>Nhặt hàng</Checkbox>
                  </Col>
                </Row>
              </Form.Item>
            </div>
            <div className="flex flex-col gap-8 ">
              <TitleModal>
                5. Cơ sở hạ tầng
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <Form.Item>
                <Row>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>
                      Hệ thống sưởi ấm, thông gió và điều hoà không khí HVAC
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>
                      Hệ thống kiểm soát độ ẩm
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>
                      Hệ thống chống bụi
                    </Checkbox>
                  </Col>
                  <Col xs={24} md={8}>
                    <Checkbox onChange={onChangeCheckBox}>
                      Hệ thống chống côn trùng
                    </Checkbox>
                  </Col>
                </Row>
              </Form.Item>
            </div>
            <div className="flex flex-col gap-8">
              <TitleModal>
                6. Điều khoản hợp đồng
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <div className="flex flex-row gap-8">
                <Form.Item>
                  <Input type="number" placeholder="0"></Input>
                </Form.Item>
                <Form.Item>
                  <Select
                    onChange={handleTypeWareChange}
                    placeholder="Vui lòng chọn"
                    optionFilterProp="label"
                    showSearch
                    onSearch={onSearch}
                    dropdownStyle={{
                      minWidth: "170px",
                      overflow: "auto",
                    }}
                    options={[
                      { value: "1", label: "Tháng" },
                      { value: "2", label: "Quý" },
                      { value: "3", label: "Năm" },
                      { value: "4", label: "Ngày" },
                    ]}
                  />
                </Form.Item>
              </div>
            </div>
            <div className="flex flex-col gap-8">
              <TitleModal>
                7. Hạn chót gửi báo giá
                <sup style={{ color: "rgb(255, 77, 79)" }}>*</sup>
              </TitleModal>
              <div className="flex flex-row gap-8">
                <Form.Item>
                  <DatePicker
                    defaultValue={dayjs("07-07-2024", "DD-MM-yyyy")}
                  ></DatePicker>
                </Form.Item>
              </div>
            </div>
            <div className="flex flex-col gap-8">
              <TitleModal>8. Ghi chú</TitleModal>
              <Form.Item>
                <TextArea placeholder="Chọn" rows={4}></TextArea>
              </Form.Item>
            </div>
          </div>
        </Form>
      </ModalContent>
    </Modal>
  );
};

export default PopupSendQuote;
