import { Row, Col, Button } from "antd";
import { CheckCircleFilled, MailFilled } from "@ant-design/icons";

import React, { memo, useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import UserLayout from "../../layouts/UserLayout/UserLayout";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";

import ProductItem from "../WarehousesPage/ProductItem";
import Kho1Image from "../../assets/kho1.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { ProductDetailStyle, GetAQuote } from "./ProductDetailPageStyled";
import PopupSendQuote from "../ProductDetailPage/PopupSendQuote";

const ProductDetailPage = (props) => {
  const { id } = useParams();
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <UserLayout showFooter={true} showSlide={false}>
      <ContentBlockWrapper>
        <ProductDetailStyle className="container-fluid mx-auto font-jakarta bg-white">
          <div className="md:px-6 px-3">
            {/* Tên */}
            <div className="pt-3 flex items-center mb-6 gap-3 flex-wrap">
              <h1 className=" text-neutral-600 font-bold tracking-wide text-[22px]  md:text-[32px] mb-0 line-clamp-2">
                Kho kiểm soát nhiệt độ, Hà Nội, WHN-4-00154
              </h1>
              <div className="verify">
                Logiware Xác Thực
                <CheckCircleFilled />
              </div>
            </div>
            {/* Ảnh */}
            <div className="hidden md:block" style={{ marginBottom: "20px" }}>
              <div className="grid md:grid-cols-2 gap-2">
                <div className="w-full">
                  <img
                    src={Kho1Image}
                    alt=""
                    className="w-full  min-h-[485px] rounded-none  object-cover"
                  />
                </div>
                <div className="grid md:grid-cols-2 gap-1 gap-y-1 min-h-[485px]">
                  <img
                    src={Kho1Image}
                    alt=""
                    className="w-full rounded-none w-90 h-60 object-cover"
                  />
                  <img
                    src={Kho1Image}
                    alt=""
                    className="w-full rounded-none w-90 h-60 object-cover"
                  />
                  <div className="relative" style={{ gridColumn: "1 / end" }}>
                    <img
                      src={Kho1Image}
                      alt=""
                      className="w-full rounded-none w-90 h-60 object-cover"
                    />
                    <span className="absolute bottom-5 right-5  text-neutral-900 w-[154px] text-center py-2">
                      <button
                        className="bg-[white]"
                        style={{ padding: "4px 8px", borderRadius: "8px" }}
                      >
                        Xem tất cả (5) Hình ảnh
                      </button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            {/* Chi tiết */}
            <Row
              style={{ marginLeft: "-8px", marginRight: "-8px", rowGap: "0px" }}
            >
              <Col
                className="warehouse-detail__warehouse-info"
                xs={24}
                md={24}
                lg={14}
                xl={15}
                style={{ marginLeft: "8px", marginRight: "8px" }}
              >
                <div className="flex-grow w-full">
                  <h1 className="h4-small-semibold">Mô tả</h1>
                  <div className="mb-6">Cung cấp dịch vụ chuyên nghiệp</div>
                  <span className="h4-small-semibold">Tổng quan</span>
                  <div className="overflow-x-auto relative rounded-2xl bg-neutral-100 border mt-5">
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                      <tbody>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg sm">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap md:w-[30%] text-sm">
                            Hồ sơ công ty
                          </td>
                          <td className="py-3 px-4 font-semibold text-sm bg-neutral-50">
                            Kho tư nhân
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            {" "}
                            Kho ngoại quan
                          </td>
                          <td className="py-3 px-4 font-semibold text-sm bg-neutral-50">
                            Không
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            Năm bắt đầu hoạt động
                          </td>
                          <td className="py-3 px-4 font-semibold text-sm bg-neutral-50">
                            2018
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            Diện tích đất
                          </td>
                          <td className="py-3 px-4 font-semibold text-sm bg-neutral-50">
                            10.000 &nbsp; m<sup>2</sup>
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            Diện tích cở sở
                          </td>
                          <td className="py-3 px-4 font-semibold text-sm bg-neutral-50">
                            2.000&nbsp; m<sup>2</sup>
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            Nhiệt độ kho (ºC)
                          </td>
                          <td className="py-3 px-4 border-b font-semibold text-sm bg-neutral-50">
                            Từ -25 <sup>o</sup>C &nbsp; Đến 25<sup>o</sup>C
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 md:text-lg">
                          <td className="py-3 px-4 text-gray-900 whitespace-nowrap text-sm">
                            Độ ẩm (%)
                          </td>
                          <td className="py-3 px-4 border-b font-semibold text-sm bg-neutral-50">
                            Từ 30 % &nbsp; Đến 60%
                          </td>
                        </tr>
                        <tr className="bg-white md:text-lg">
                          <td className="py-3 rounded-bl-2xl  px-6 text-gray-900 whitespace-nowrap text-sm">
                            Dấu Nhãn (Tags)
                          </td>
                          <td className="py-3 px-4 text-4 font-semibold text-sm bg-neutral-50">
                            Kho vệ tinh, Cấp độ 1 - Không tự động
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Các danh mục sản phẩm
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hàng hoá nông nghiệp
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Trang phục, Dệt may & Phụ kiện
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Ôtô & Vận tải
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Túi xách, Giày dép & Phụ kiện
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Nguyên vật liệu công nghiệp
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Thiết Bị & Linh Kiện Điện
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Thiết bị điện tử
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Quà tặng, Thể thao & Đồ chơi
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Các chứng nhận
                  </div>
                  <div className="grid md:grid-cols-3 grid-cols-1  gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Cấp thực phẩm(Chứng nhận an toàn vệ sinh thực phẩm)
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Cấp cồn(Giấy phép kinh doanh)
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Cấp dược phẩm (Chứng nhận GSP)
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Cấp nguyên vật liệu nguy hiểm(Tiêu chuẩn quốc gia)
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Dịch vụ kho bãi
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Dịch vụ Fulfillment
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Xếp dỡ hàng hóa
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Kiểm duyệt hàng trước khi nhập kho
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Xếp hàng lên pallet
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Bọc màng co cho pallet
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Nhập hàng
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Cách thức lưu kho
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Pallet xếp chồng đơn trên sàn
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Pallet xếp chồng đôi trên sàn
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Pallet sắp xếp trên kệ
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hàng rời để trên sàn
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Cơ sở hạ tầng
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống sưởi ấm, thông gió và điều hoà không khí
                            HVAC
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống kiểm soát độ ẩm
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống chống bụi
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống chống côn trùng
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    An ninh và An toàn
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống báo động
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Đầu phun PCCC
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Camera an ninh
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Trụ nước PCCC
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-6 flex">
                    Thiết bị điện tử và tự động hoá
                  </div>
                  <div className="grid md:grid-cols-2 grid-cols-1 gap-2 items-center py-5 md:py-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-3 col-span-2 w-full">
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <CheckCircleFilled />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Hệ thống barcode
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Máy scan
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Máy quét cầm tay
                          </span>
                        </div>
                      </Col>
                      <Col
                        xs={24}
                        md={24}
                        style={{ paddingLeft: "8px", paddingRight: "8px" }}
                      >
                        <div className="inline-flex items-start">
                          <FontAwesomeIcon icon={faCircleXmark} />
                          <span className="pl-1 text-neutral-500 text-[14px]">
                            Máy in nhãn
                          </span>
                        </div>
                      </Col>
                    </div>
                  </div>
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-3">
                    <div>
                      <div className="h4-small-semibold text-neutral-700 mt-5">
                        Tiết kiệm năng lượng, thân thiện môi trường
                      </div>
                      <div className="grid md:grid-cols-1  gap-2 items-center py-5 md:py-3">
                        <div className="grid grid-cols-1 md:grid-cols-1 gap-3  w-full">
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Khu tái chế
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Hệ thống đèn tiết kiệm năng lượng
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Năng lượng mặt trời
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Chứng nhận LEED
                              </span>
                            </div>
                          </Col>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className="h4-small-semibold text-neutral-700 mt-5">
                        Tiện nghi
                      </div>
                      <div className="grid md:grid-cols-1  gap-2 items-center py-5 md:py-3">
                        <div className="grid grid-cols-1 md:grid-cols-1 gap-3  w-full">
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Văn phòng
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Wifi cho khách
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Khu vực đỗ xe cá nhân
                              </span>
                            </div>
                          </Col>
                          <Col
                            xs={24}
                            md={24}
                            style={{ paddingLeft: "8px", paddingRight: "8px" }}
                          >
                            <div className="inline-flex items-start">
                              <CheckCircleFilled />
                              <span className="pl-1 text-neutral-500 text-[14px]">
                                Chứng nhận LEED
                              </span>
                            </div>
                          </Col>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="h4-small-semibold text-neutral-700 mt-5 mb-4">
                    Sức chứa nhà kho
                  </div>
                  <div className="overflow-x-auto relative rounded-2xl bg-neutral-100 border">
                    <table className="tbl-detail w-full text-sm text-left text-neutral-700 dark:text-gray-400">
                      <tbody>
                        <tr className="bg-white border-b border-neutral-200">
                          <td className="py-2 px-3 w-12 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Tổng công suất chứa
                          </td>
                          <td className="bg-neutral-50 py-2 px-3 md:py-3 md:px-6 text-4 p2-semibold w-full">
                            5.000 CBM (m3)
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200">
                          <td className="py-2 px-3 w-12 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Năng lực vận hành (vào)
                          </td>
                          <td className="bg-neutral-50 py-3 px-6 text-4 p2-semibold">
                            1.000 Pallet
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200  ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Năng lực vận hành (ra)
                          </td>
                          <td className="bg-neutral-50 py-3 px-6 text-4 p2-semibold">
                            100 Pallet
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200  ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Số lượng cổng vào
                          </td>
                          <td className="bg-neutral-50 py-2 px-3 md:py-3 md:px-6 p2-semibold">
                            3
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200  ">
                          <td className="py-2 px-3 md:py-3 md:px-6  p2-regular text-gray-900 whitespace-nowrap">
                            Số lượng cổng ra
                          </td>
                          <td className="bg-neutral-50 py-3 px-3 md:px-6 border-b p2-semibold">
                            3
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200   p2-semibold">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Nhà kho
                          </td>
                          <td className="bg-neutral-50 py-3 px-3 md:px-6 border-b p2-semibold">
                            Khác
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200  ">
                          <td className="py-2 px-3 md:py-3 md:px-6 rounded-bl-2xl p2-regular text-gray-900 whitespace-nowrap">
                            Chiều cao tính bằng mét
                          </td>
                          <td className="bg-neutral-50 py-3 px-3 md:px-6 p2-semibold">
                            15
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200  ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Kích thước pallet tối đa (DxRxC)
                          </td>
                          <td className="bg-neutral-50 py-2 px-3 md:py-3 md:px-6 border-b p2-semibold">
                            <div className="inline-flex gap-2">
                              <span className="inline-flex items-center gap-3">
                                1<span className="text-xs">X</span>
                              </span>
                              <span className="inline-flex items-center gap-3 ">
                                1<span className="text-xs">X</span>
                              </span>
                              <span>1.5</span>
                              <span>m</span>
                            </div>
                          </td>
                        </tr>
                        <tr className="bg-white border-b border-neutral-200 ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Trọng lượng pallet tối đa (kg)
                          </td>
                          <td className="py-3 px-3 md:px-6 border-b p2-semibold bg-neutral-50">
                            500
                          </td>
                        </tr>

                        <tr className="bg-white ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Ngày và giờ hoạt động
                          </td>
                          <td className="py-3 px-3 md:px-6 border-b p2-semibold bg-neutral-50">
                            <div className="bePwQhvh vZ3bq5rm">
                              <span> Thứ Năm:</span>
                              <span> 08:00 - 17:00</span>
                              <span
                                role="img"
                                aria-label="caret-down"
                                className="anticon anticon-caret-down"
                              >
                                <svg
                                  viewBox="0 0 1024 1024"
                                  focusable="false"
                                  data-icon="caret-down"
                                  width="1em"
                                  height="1em"
                                  fill="currentColor"
                                  aria-hidden="true"
                                >
                                  <path d="M840.4 300H183.6c-19.7 0-30.7 20.8-18.5 35l328.4 380.8c9.4 10.9 27.5 10.9 37 0L858.9 335c12.2-14.2 1.2-35-18.5-35z"></path>
                                </svg>
                              </span>
                            </div>
                          </td>
                        </tr>
                        <tr className="bg-white  ">
                          <td className="py-2 px-3 md:py-3 md:px-6 p2-regular text-gray-900 whitespace-nowrap">
                            Làm việc xuyên ngày nghỉ
                          </td>
                          <td className="py-3 px-3 md:px-6 border-b p2-semibold bg-neutral-50">
                            Không
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </Col>
              <Col
                xs={24}
                md={24}
                lg={10}
                xl={8}
                style={{ marginLeft: "8px", marginRight: "8px" }}
              >
                <GetAQuote>
                  <div className="title">
                    <span>Nhận báo giá</span>
                    <div className="quote-content">
                      <div className="flex flex-col gap-16">
                        <span className="text-tertiary color-neutral-600">
                          Để nhận được báo giá, chỉ cần điền vào mẫu dưới đây
                          với một số thông tin. Nhóm Logiwaer của chúng tôi sẽ
                          liên hệ lại với bạn với báo giá ước tính ngay lập tức!
                        </span>
                        <span className="text-tertiary color-neutral-600 text-600">
                          Nhà kho: WHN-4-00154
                        </span>
                      </div>
                      <Button
                        type="primary"
                        size="lg"
                        onClick={() => setIsModalOpen(true)}
                      >
                        <MailFilled />
                        <span>Báo giá nhanh</span>
                      </Button>
                    </div>
                  </div>
                </GetAQuote>
              </Col>
            </Row>
            {/* Vị trí Time lái xe */}
            <div></div>
          </div>
          <div className="warehouse-detail__similar-options py-8 hidden md:block">
            <h1 className="h3-semibold">Tùy chọn tương tự</h1>
            <div className="similar-options__container mt-4 gap-3">
              <ProductItem />
              <ProductItem />
            </div>
          </div>
        </ProductDetailStyle>
        <PopupSendQuote
          isModalOpen={isModalOpen}
          handleCancel={() => setIsModalOpen(false)}
        ></PopupSendQuote>
      </ContentBlockWrapper>
    </UserLayout>
  );
};

export default ProductDetailPage;
