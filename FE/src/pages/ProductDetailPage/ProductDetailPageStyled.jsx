import { Card, Col, List } from "antd";
import styled, { css } from "styled-components";
export const ProductDetailStyle = styled.div`
  max-width: 1120px;
  .warehouse-detail__similar-options .similar-options__container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
  }

  @media only screen and (min-width: 768px) {
    .warehouse-detail__similar-options .similar-options__container {
      grid-template-columns: repeat(2, 1fr);
    }
  }

  @media only screen and (min-width: 1200px) {
    .warehouse-detail__similar-options .similar-options__container {
      grid-template-columns: repeat(4, 1fr);
    }
  }
  .verify {
    display: flex;
    align-items: center;
    font-weight: 700;
    color: #047857;
    background-color: #ecfdf5;
    border-radius: 20px;
    padding: 6px 14px;
    height: 30px;
    width: -moz-fit-content;
    width: fit-content;
  }
`;
export const GetAQuote = styled.div`
  width: 436px;
  max-width: 100%;
  border-radius: 8px;
  background-color: #e2e8f0;
  height: fit-content;
  position: sticky;
  top: 80px;
  .title {
    padding: 16px;
    text-align: center;
  }
  .quote-content {
    display: flex;
    flex-direction: column;
    gap: 24px;
    padding: 16px;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }
`;
export const ModalContent = styled.div`
  max-height: min(650px, 64vh) !important;
  height: -moz-fit-content;
  height: fit-content;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;
  padding: 0 24px !important;
`;
export const TitleModal = styled.div`
  color: var(--secondary-color-500);
  font-size: 16px;
  font-weight: 700;
`;
