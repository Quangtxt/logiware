import { Card, Col, List } from "antd";
import styled, { css } from "styled-components";
export const Layout = styled.div`
  .form_search {
    background-color: #fff;
    position: sticky;
    z-index: 100;
    top: 48px;
    padding: 12px 24px 24px;
    width: 100%;
  }
`;
export const FilterBar = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  gap: 16px;
  overflow-y: auto;
  align-items: center;
  .content {
    height: 100%;
    background-color: #fff;
    padding: 0 24px;
  }
  @media only screen and (min-width: 1200px) {
    .content {
      padding: 0 40px;
    }
  }
`;
export const SearchBar = styled.div`
  display: flex;
  flex-direction: row;
  gap: 8px;
  align-items: center;
`;
export const ContentBar = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
  margin-bottom: 16px;
`;
export const ProductItemOne = styled.div`
  max-width: 350px;
  position: relative;
  a {
    color: inherit;
    text-decoration: none;
  }
`;
export const ItemFooter = styled.div`
  margin-top: 8px;
  a {
    color: inherit;
    text-decoration: none;
  }
  .card-body__name-container {
    display: grid;
    grid-template-columns: auto 55px;
    gap: 4px;
    color: #000;
  }
  .card-body__name {
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
  }
  .service {
    display: grid;
    grid-template-columns: 50% 50%;
    justify-content: space-between;
    align-items: center;
    margin-top: 8px;
  }
  .description-price {
    text-align: right;
  }
  .card-body__width {
    color: var(--neutral-500);
    font-size: 14px;
  }
  .card-body__price {
    color: var(--primary-color);
    font-weight: 700;
    font-size: 18px !important;
    margin-right: 8px;
  }
  .description-price__unit {
    display: flex;
    justify-content: end;
    position: relative;
    top: -4px;
  }
`;
export const VacancyRate = styled.div`
  color: var(--neutral-500);
  font-size: 14px;
  gap: 2px;
  display: flex;
  margin-bottom: 0.5rem;
`;
export const ListService = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
`;
export const ImageContent = styled.div`
  width: 100%;
  aspect-ratio: 20 / 19;
  position: relative;
  *,
  img {
    border-radius: 12px;
    width: 100%;
    height: 100%;
    position: absolute;
    -o-object-fit: cover;
    object-fit: cover;
  }
  .code_content {
    width: auto;
    z-index: 10;
    position: absolute;
    right: 10px;
    top: 10px;
    height: auto;
  }
  .code_content .code {
    position: relative;
    background-color: #fff !important;
    height: 24px;
    padding: 4px 16px;
    width: auto;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
export const GridItem = styled.div`
  display: grid;
  gap: 16px;
  grid-template-columns:
    1fr
    @media only screen and (min-width: 576px) {
    grid-template-columns: 1fr 1fr;
  }

  @media only screen and (min-width: 1366px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media only screen and (min-width: 640px) {
    grid-template-columns: 1fr 1fr;
  }

  @media only screen and (min-width: 992px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media only screen and (min-width: 1200px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }

  @media only screen and (min-width: 1920px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  }
`;
