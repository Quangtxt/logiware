import {
  Form,
  Input,
  Select,
  Dropdown,
  Menu,
  Radio,
  Space,
  Row,
  Col,
  Progress,
  Carousel,
  Button,
} from "antd";
import {
  SearchOutlined,
  DownOutlined,
  TableOutlined,
  ThunderboltOutlined,
  StarFilled,
  SlidersFilled,
  FacebookFilled,
} from "@ant-design/icons";

import React, { memo, useEffect, useState } from "react";

import {
  Layout,
  FilterBar,
  SearchBar,
  ContentBar,
  GridItem,
} from "./WarehousesPageStyled";
import ProductItem from "./ProductItem";
const { Option } = Select;

const WarehousesPage = (props) => {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values of form:", values);
  };

  const handlePriceDropdown = (e) => {
    console.log("Price dropdown clicked:", e);
  };
  return (
    <Layout>
      <Form
        id="MARKETPLACE_WAREHOUSE_FILTER"
        form={form}
        name="MARKETPLACE_WAREHOUSE_FILTER"
        onFinish={onFinish}
        layout="horizontal"
        className="form_search"
      >
        <div style={{ fontSize: 30, fontWeight: 500, marginBottom: 8 }}>
          Kho dịch vụ
        </div>
        <FilterBar>
          <SearchBar>
            <Form.Item
              name="searchText"
              className="css-1c6nj5r"
              style={{ width: 320, marginBottom: "0px" }}
            >
              <Input placeholder="Tìm kiếm" prefix={<SearchOutlined />} />
            </Form.Item>
            <Form.Item
              name="typeFilterIds"
              className="css-1c6nj5r"
              style={{ minWidth: 280, marginBottom: "0px" }}
            >
              <Select
                mode="multiple"
                placeholder="Loại kho"
                showSearch
                optionLabelProp="label"
              >
                {/* Add options here */}
              </Select>
            </Form.Item>
            <Form.Item
              name="provinceId"
              className="css-1c6nj5r"
              style={{ minWidth: 210, marginBottom: "0px" }}
            >
              <Select
                showSearch
                placeholder="Nhập địa điểm"
                optionLabelProp="label"
              >
                {/* Add options here */}
              </Select>
            </Form.Item>
            <Button trigger={["click"]}>
              <Space>
                <span>Giá</span>
                <DownOutlined />
              </Space>
            </Button>
            <span style={{ fontSize: 14, color: "#94a3b8" }}>
              * Giá tham khảo
            </span>
          </SearchBar>
          <Form.Item name="layout" className="IqY5Gm9x css-1c6nj5r">
            <Radio.Group id="layout" className="QmqptOO8 css-1c6nj5r">
              <Radio.Button value="list" className="Yfd49KHQ css-1c6nj5r">
                <TableOutlined />
              </Radio.Button>
              <Radio.Button value="map" className="Yfd49KHQ css-1c6nj5r">
                <ThunderboltOutlined />
              </Radio.Button>
            </Radio.Group>
          </Form.Item>
        </FilterBar>
      </Form>
      <div className="content">
        <GridItem>
          <ProductItem />
          <ProductItem />
          <ProductItem />
          <ProductItem />
          <ProductItem />
        </GridItem>
        {/* <Row>
          <Col xs={24} xl={24}>
            <ContentBar>
              <>
                <Row>
                  <Col xs={24} sm={24} md={12} lg={8} span={6}>
                    <ProductItem />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} span={6}>
                    <ProductItem />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} span={6}>
                    <ProductItem />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} span={6}>
                    <ProductItem />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} span={6}>
                    <ProductItem />
                  </Col>
                </Row>
              </>
            </ContentBar>
          </Col>
        </Row> */}
      </div>
    </Layout>
  );
};

export default WarehousesPage;
