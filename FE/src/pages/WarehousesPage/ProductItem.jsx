import React, { memo, useEffect, useState } from "react";
import { Progress, Carousel } from "antd";
import { StarFilled, SlidersFilled, FacebookFilled } from "@ant-design/icons";
import {
  ProductItemOne,
  ItemFooter,
  ListService,
  VacancyRate,
  ImageContent,
} from "./WarehousesPageStyled";
import heroImage from "../../assets/hero.png";

const ProductItem = (props) => {
  return (
    <ProductItemOne>
      <a href="warehouse/1">
        <ImageContent>
          <div className="code_content">
            <span
              className="kVPknXuM __0Nbpf5By mVbGNVn7 code"
              style={{
                color: "rgb(10, 152, 48)",
                backgroundColor: "rgb(241, 245, 249)",
                borderColor: "rgb(10, 152, 48)",
              }}
            >
              WHN-3-00441
            </span>
          </div>
          {/* <Carousel>
            {images.map((image, index) => (
              <img key={index} src={image}></img>
            ))}
          </Carousel> */}
          <img src={heroImage}></img>
        </ImageContent>
      </a>
      <ItemFooter>
        <a href="warehouse/1">
          <div className="card-body__name-container">
            <span className="card-body__name">
              [Hải Phòng] Kho thường (có kệ)
            </span>
            <span
              className="card_start"
              style={{
                display: "flex",
                alignItems: "center",
                gap: ".25rem",
              }}
            >
              <StarFilled />
              4.95
            </span>
          </div>
        </a>
        <div className="service">
          <div>
            <VacancyRate>
              <SlidersFilled />
              <Progress
                percent={61}
                showInfo={true}
                strokeColor="#52c41a" // Màu xanh lá cây
                trailColor="#595959" // Màu xám tối
              />
            </VacancyRate>
            <ListService>
              <FacebookFilled />
              <FacebookFilled />
              <FacebookFilled />
              <FacebookFilled />
              <FacebookFilled />
              <FacebookFilled />
            </ListService>
          </div>
          <div className="card-body__width description-price">
            <span className="card-body__price">1.000.000₫</span>
            <br />
            <div className="description-price__unit">
              <div>container (teu)</div>
              <span> / Tháng</span>
            </div>
          </div>
        </div>
      </ItemFooter>
    </ProductItemOne>
  );
};

export default ProductItem;
